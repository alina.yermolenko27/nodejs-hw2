const express = require('express');
const morgan = require('morgan')
const app = express();
const mongoose = require("mongoose");
const dotenv = require("dotenv");

dotenv.config({path: './.env'});


const { filesRouter } = require('./filesRouter.js');
const PORT = process.env.PORT || 8080;


app.use(express.json());
app.use(morgan('tiny'));

const cors = require('cors');
app.use(cors());

app.use('/api', filesRouter);

const start = async () => {
  try {
    app.listen(PORT);
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
}

start();

//ERROR HANDLER
app.use(errorHandler)

function errorHandler(err, req, res, next) {
  console.error('err')
  res.status(500).send({ 'message': 'Server error' });
}



mongoose
  .connect(process.env.MONGODB_URL, {
    useNewUrlParser: true,
    // useUnifiedTopology: true,
  })
  .then(() => {
    console.log("Connected to MongoDB");
  })
  .catch((err) => {
    console.log(err);
  });



