const express = require('express');
const router = express.Router();

const { authMiddleware } = require('./middleware/authMiddleware');
const { getUser, deleteUser, changeUserPassword } = require("./controllers/user");
const { getNotes, createNote, getNoteById, updateNoteById, checkNoteById, deleteNoteById } = require("./controllers/notes");
const { registerUser, loginUser } = require("./controllers/auth");

router.post('/auth/register', registerUser);
router.post('/auth/login', loginUser);

router.get('/users/me', authMiddleware, getUser);
router.delete('/users/me', authMiddleware, deleteUser);
router.patch('/users/me', authMiddleware, changeUserPassword);


router.get('/notes', authMiddleware, getNotes);
router.post('/notes', authMiddleware, createNote);
router.get('/notes/:id', authMiddleware, getNoteById);
router.put('/notes/:id', authMiddleware, updateNoteById);
router.patch('/notes/:id', authMiddleware, checkNoteById);
router.delete('/notes/:id', authMiddleware, deleteNoteById);




module.exports = {
  filesRouter: router
};
