const User = require("../models/userModel");
const bcrypt = require("bcryptjs");

const jwt = require('jsonwebtoken');
const { ObjectID } = require("bson");

// let button = document.getElementsByClassName('create-user')


const getUser = async (req, res) => {
  try {
    let user = await User.findOne({ "_id": ObjectID(req.user.userId) });
    if (user) {
      return res.status(200).json({
        "user": {
          "_id": user._id,
          "username": user.username,
          "createdDate": user.createdDate,
        }
      });
    }
    else {
      return res.status(400).json({ 'message': 'User not found' });
    }
  } catch (err) {
    return res.status(500).json({ 'message': 'Server error' });
  }

};

const deleteUser = async (req, res) => {
  let user = await User.collection.deleteOne({ "_id": ObjectID(req.user.userId) })
  if (user) {
    return res.status(200).json({
      "message": "Success",
    });
  }
  if (!user) {
    return res.status(400).json({ 'message': 'Bad request' });
  } else {
    return res.status(500).json({ 'message': 'Server error' });
  }
};

const changeUserPassword = async (req, res) => {
  // const user = await User.findOneAndUpdate({ "_id": ObjectID(req.user.userId) }, { password: await bcrypt.hash(req.body.newPassword, 10) });
  let currentUser = await User.findOne({ "_id": ObjectID(req.user.userId) });
  if (await bcrypt.compare(String(req.body.oldPassword), String(currentUser.password))) {

    await User.updateOne(
      { "_id": ObjectID(req.user.userId) },
      { password: await bcrypt.hash(req.body.newPassword, 10) }
    );

    return res.status(200).json({ 'message': 'Successfully changed' })
  }
  if (!req.body.newPassword) {
    return res.status(403).json({ 'message': 'Write a new password' });
  } else {
    return res.status(403).json({ 'message': 'Wrong password' });
  }
};

// let register = button.addEventListener('click', RegisterUser)

module.exports = {
  getUser,
  deleteUser,
  changeUserPassword
};