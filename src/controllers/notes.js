const Note = require("../models/noteModel");
const bcrypt = require("bcryptjs");
const User = require("../models/userModel");
const mongoose = require("mongoose");


const jwt = require('jsonwebtoken');
const { ObjectID } = require("bson");


const getNotes = async (req, res) => {
  if (req.query.offset === undefined) {
    req.query.offset = 0 || +req.query.offset;
  }
  if (req.query.limit === undefined) {
    req.query.limit = 100 || +req.query.limit;
  }
  try{
    console.log(req)
    const notes = await Note.find({ userId: req.user.userId }, '-__v')

    console.log(notes.length)
    return res.status(200).json(
      {
        offset: req.query.offset !== undefined ? +req.query.offset : undefined,
        limit: req.query.limit !== undefined ? +req.query.limit : undefined,
        count: notes.length,
        notes: notes
      }
    )
  }catch(err){
    return res.status(400).json({message: err.message});
  }
}

const createNote = async (req, res) => {
  let user = await User.findOne({ "_id": ObjectID(req.user.userId) });
  const { text } = req.body;
  if (user) {
    console.log(user)
    let userNote = new Note({
      _id: user.userId,
      userId: user._id,
      text: text,
      createdDate: new Date().toISOString(),
    });

    return userNote.save()
      .then(saved => console.log(saved))
      .then(
        res.status(200).send({ "message": `Success` })
      )
      .catch(err => {
        console.log(err)
      })
  }
  if (!user) {
    res.status(400).send({ "message": `User doesn't exist` })
  } else {
    res.status(500).send({ "message": `server error` })

  }
}

const getNoteById = async (req, res) => {
  // let user = await Note.findOne({ "userId": ObjectID(req.user.userId) });
  try {
    let note = await Note.findOne({ "_id": ObjectID(req.params.id) });
    if (note) {
      res.status(200).send({
        note: note,
      })
    }
    if (!note) {
      res.status(400).send({ "message": `Note doesn't exist` })
    }
  } catch (err) {
    res.status(500).send({ "message": `server error` })
  }
}

const updateNoteById = async (req, res) => {
  try {
    let note = await Note.findOneAndUpdate({ "_id": ObjectID(req.params.id) }, { "text": req.body.text });
    if (note) {
      return res.status(200).json({ 'message': 'Successfully changed' })
    }
    if (!req.body.text) {
      return res.status(403).json({ 'message': 'No text' });
    }
  } catch (err) {
    return res.status(403).json({ 'message': 'Error' });
  }

}

const checkNoteById = async (req, res) => {
  try {
    let note = await Note.findOneAndUpdate({ "_id": ObjectID(req.params.id) }, { "completed": !req.params.completed });
    if (note) {
      return res.status(200).json({ 'message': 'Successfully changed' })
    }
    if (!req.body.text) {
      return res.status(403).json({ 'message': 'No text' });
    }
  } catch (err) {
    return res.status(403).json({ 'message': 'Error' });
  }
}

const deleteNoteById = async (req, res) => {
  console.log('fff')
  try {
    console.log(req.note)
    let note = await Note.collection.deleteOne({ "_id": ObjectID(req.params.id) });
    console.log(ObjectID(req.params.id) === req.params.id)
    if (note) {
      return res.status(200).send({ "message": "Successfully deleted note" })
    }
    if (!note) {
      return res.status(400).send({ "message": `Note doesn't exist` })
    }
  }
  catch (err) {
    return res.status(500).send({ "message": `server error` })
  }

}



module.exports = {
  getNotes,
  createNote,
  getNoteById,
  updateNoteById,
  checkNoteById,
  deleteNoteById
};