const jwt = require('jsonwebtoken');
const User = require("../models/userModel");


const authMiddleware = async (req, res, next) => {
  const { authorization } = req.headers;

  if (!authorization) {
    return res.status(401).json({ 'message': 'Please, provide authorization' });
  }
  const token = authorization.split(' ')[1];
  if (!token) {
    return res.status(401).json({ 'message': 'Please, include token to request' });
  }

  try {
    const tokenPayLoad = jwt.verify(token, process.env.JWT_SECRET_KEY);
    const currentUser = await User.findById(tokenPayLoad._id);
  
  req.user = {
    userId: tokenPayLoad._id,
    username: currentUser.username,
  }
 
    next();
  } catch (err) {
    return res.status(401).json({ 'message': err.message });
  }

}

module.exports = {
  authMiddleware
}
